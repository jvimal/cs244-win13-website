#!/usr/bin/python

import cgi
import csv
import os
import sys

COUPON_FILE = "coupons"
TITLE = "EC2 Coupon Code"
MESSAGE = """
<p>
Hi, %s.
</p>

<p>
Here's your EC2 coupon code: <code>%s</code>
</p>

<p>
Instructions on how to use the code are on the class website:
<a href="http://www.stanford.edu/class/cs244/2013/ec2.html">
http://www.stanford.edu/class/cs244/2013/ec2.html
</a>
</p>

<p>Remember to STOP (not terminate) your instance when you're not
using it to not burn through your credits.  Any overcharges will be
billed to YOUR CREDIT CARD!!</p>

<p>For students who took CS144 in Fall 2012: if you are unable to use
this coupon code, try again using a new Amazon account.  If that
fails, inform the TAs and we will help you sort it out.</p>

<p>
Happy programming!
</p>
"""

print 'Content-Type: text/html'
print

reader = csv.reader(open(COUPON_FILE, 'rb'))
ec2_coupons = dict()

for row in reader:
    suid = row[0]
    ec2_coupons[suid] = row[1]

print '<html>'
print '<head><title>%s</title></head>' % TITLE
print '<body style="width:500px">'

try:
    username = os.environ['WEBAUTH_USER']
    name = os.environ['WEBAUTH_LDAP_DISPLAYNAME']
    coupon = ec2_coupons[username]
    print MESSAGE % (name, coupon)
except KeyError:
    print "<p>Either you are not a student of this class"
    print "OR your class registration has not been verified.  Please wait"
    print "24 hours for verification.</p>"
    print "<p>If you think this is an error, please contact the TAs IMMEDIATELY."
    print "  Thanks. </p>"
print "</body>"
print "</html>"
