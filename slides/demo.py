import random
import math
import pdb


def mynewmain():
  a = 4
  print a

  a = 'a string'
  a = '''a multiline 
  string'''

  print a
  #print 'Press enter to continue execution'
  #raw_input()

  a='''here "indentation" is 
       part of the string, which is
       ignored by python'''



  #Forward definition is not needed when using the "kickstart" method (__main__)
  m = mymax(4, 5)
  print m

  mylist = [random.random() for i in range(1,10)]
  print mylist
  #raw_input()

  stats = mystats(mylist)
  print stats

  #Tuple unpacking
  mean, std = mystats(mylist)
  print 'Mean: %s' % mean
  print 'Std:', std
  print 'Mean, std: ', mean, std


  #List zipping
  l1 = [random.randint(1,10) for i in range(10)]
  print l1

  print 'Press enter to continue execution'
  raw_input()

  #In place sort
  #l1.sort()
  #print l1
  #print 'Press enter to continue execution'
  #raw_input()

  #More sorting examples
  l1s = sorted(l1,reverse=True)
  print l1s
  print l1


  #pdb.set_trace()

  l2 = 'abcdefghij'
  print type(l2)

  print l2[2]

  print 'Press enter to continue execution'
  raw_input()
  l = zip(l1, l2)
  print l
  print 'Press enter to continue execution'
  raw_input()

  #Sorting a list of tuples
  ls = sorted(l)
  print ls

  #Sorting a list of tuples by chosen key (think column)
  ls = sorted(l,key=lambda tup: tup[1])
  print ls
  print 'Press enter to continue execution'
  raw_input()


def mymax(a, b):
  #Ternary expressions are allowed in recent python version but have a funny syntax.
  #The python expression  a if a > b else b  is the same as C equivalent  a > b ? a : b
  return a if a > b else b

#Another example of using ternary expressions; also use a list sum and length functions.
def mymean(inplist):
  mean = sum(inplist)/len(inplist) if len(inplist) > 0 else float('nan')
  return mean

#Compute and return mean and standard deviation with built-in functions using variance = E[X^2] - E[X]^2
#"Multiple return values" in the form of a tuple -- very convenient!
def mystats(inplist):
  #Compute expectation of X (mean of the list)
  EX = mymean(inplist)

  #Compute expectation of X^2 (mean of the list-squared)
  #BE CAREFUL! The power operator is ** which is ^ or .^ in MATLAB. ^ is the bitwise-XOR operator.
  EX2 = mymean([x**2 for x in inplist])

  #Semicolons are fine at the end of statements but are optional.
  variance = EX2 - EX**2;

  std = math.sqrt(variance)

  #Multiple return values are returned as a single tuple.
  #return (EX, std)

  #This has the same effect.
  return EX, std



if __name__=="__main__":
   mynewmain()
