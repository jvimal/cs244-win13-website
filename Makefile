DOCS=index honorcode mininet ec2 pa1 pa2 pa3 scpd scribe-assignment sessions staff timetable critique pa3-teams

HDOCS=$(addsuffix .html, $(DOCS))
PHDOCS=$(addprefix html/, $(HDOCS))

.PHONY : docs
docs : $(PHDOCS) copyslides

.PHONY : update
update : $(PHDOCS)
	@echo -n 'Copying to server...'
	# insert code for copying to server here.
	#ssh cardinal.stanford.edu "rm -rf /afs/ir/class/cs244/WWW/jemdoc/*"
	scp -r html/* cardinal.stanford.edu:/afs/ir/class/cs244/WWW/2013/
	@echo ' done.'

.PHONY : uphtml
uphtml : $(PHDOCS)
	@echo -n "Copying only html files to server..."
	@echo -n "Remember to git pull before pushing your changes!"
	scp -r html/*.html cardinal.stanford.edu:/afs/ir/class/cs244/WWW/2013/
	@echo ' done.'

.PHONY : copyslides
copyslides:
	if [ -d exam ]; then cp -Rf exam html/; fi
	if [ -d papers ]; then cp -Rf papers html/; fi
	if [ -d slides ]; then cp -Rf slides html/; fi
	if [ -d slides-protected ]; then cp -Rf slides-protected html/; fi

html/%.html : %.jemdoc MENU
	# create the directory if it doesn't exist.
	mkdir -p html
	python jemdoc.py -o $@ $<
	# ugly hack to handle latex images
	if [ -d eqs ]; then cp -Rf eqs/ html/eqs/; fi
	# add images
	if [ -d images ]; then cp -Rf images html/; fi
	cp jemdoc.css html/jemdoc.css

.PHONY : clean
clean :
	-rm -rf html/*.html eqs
