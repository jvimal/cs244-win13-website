#!/usr/bin/python

import cgi
import csv
import os
import sys

COUPON_FILE = "feedback.csv"
TITLE = "PA1 Feedback"
MESSAGE = """
<p>
Hi, %(name)s.
</p>

<p>
Here is the split up for your PA1 grades.
</p>

<pre>
Working code:        %(code)2s / 4
Questions in README: %(questions)2s / 4
Correct graphs:      %(graphs)2s / 1
Code/Quality:        %(quality)2s / 1
--------------------------------
Total:               %(total)2s /10
</pre>

Comments/Feedback:
<pre>
%(comment)s
</pre>
"""

print 'Content-Type: text/html'
print

reader = csv.reader(open(COUPON_FILE, 'rb'))
feedback = dict()
DEFAULT_COMMENT="Nothing specific."

for row in reader:
    suid = row[0]
    message = dict(code=row[1],
                   questions=row[2],
                   graphs=row[3],
                   quality=row[4],
                   total=row[6],
                   comment=row[7])
    feedback[suid] = message

print '<html>'
print '<head><title>%s</title></head>' % TITLE
print '<body style="width:500px">'

try:
    username = os.environ['WEBAUTH_USER']
    name = os.environ['WEBAUTH_LDAP_DISPLAYNAME']
    values = feedback[username]
    if values.get("comment", '') == '':
        values["comment"] = DEFAULT_COMMENT
    values.update(dict(name=name))
    print MESSAGE % values
except KeyError:
    print "<p>Either you are not a student of this class"
    print "OR your assignment was not graded."
    print "<p>If you think this is an error, please contact the TAs IMMEDIATELY."
    print "  Thanks. </p>"
print "</body>"
print "</html>"
