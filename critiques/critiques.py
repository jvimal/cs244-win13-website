#!/usr/bin/env python
import gdata.spreadsheet.service
from string import find
from papers import papers
import string
import pickle
from optparse import OptionParser
import code

# Official Docs
FRESH_CRITIQUES_KEY="t4yMUYMEMCLbW7AH_3G7u6w"
SUBMITTED_CRITIQUES_KEY="t1xFjOezjfj115-j07UxDug"

# Docs Keys
SUBMITTED_CRITIQUES_KEY="0AmavjfqTpYCadGdnSEtMRlhWQ2UxZkplcURFOGstZ1E"

# Test Docs
#FRESH_CRITIQUES_KEY="tvKUx6RH7Y78fza_xB42N2g"
#FRESH_CRITIQUES_WKSH_ID="od6"
#SUBMITTED_CRITIQUES_KEY="tSDvYsXqrOzmZrQ5tcoDX4A"

USERNAME="cs244stanford"
PASSWORD="openflow"

PAPERLIST="paperlist.txt"

def create_sheets():
    full_papers = []
    for paper in papers:
        worksheet = client.AddWorksheet(str(paper[0]),100,10,SUBMITTED_CRITIQUES_KEY)
        worksheet_id = worksheet.id.text.rsplit('/',1)[1]
        client.UpdateCell(1,1,"Timestamp",SUBMITTED_CRITIQUES_KEY,worksheet_id)
        client.UpdateCell(1,2,"Name",SUBMITTED_CRITIQUES_KEY,worksheet_id)
        client.UpdateCell(1,3,"Stanford ID",SUBMITTED_CRITIQUES_KEY,worksheet_id)
        client.UpdateCell(1,4,"Reading",SUBMITTED_CRITIQUES_KEY,worksheet_id)
        client.UpdateCell(1,5,"Critique",SUBMITTED_CRITIQUES_KEY,worksheet_id)
        client.UpdateCell(1,6,"Grade",SUBMITTED_CRITIQUES_KEY,worksheet_id)
        
        full_papers.append((paper[0],worksheet_id,paper[1]))
        print "%d,%s,%s" % (paper[0],worksheet_id,paper[1])
    f=open(PAPERLIST,'w')
    pickle.dump(full_papers,f)
    f.close()

def add_new_paper(paper_index):
    f = open(PAPERLIST,'r')
    full_papers = pickle.load(f)
    f.close()
    i = 1
    for paper in papers:
        if (paper[0] == paper_index):
            worksheet = client.AddWorksheet(str(paper[0]),100,10,SUBMITTED_CRITIQUES_KEY)
            worksheet_id = worksheet.id.text.rsplit('/',1)[1]
            client.UpdateCell(1,1,"Timestamp",SUBMITTED_CRITIQUES_KEY,worksheet_id)
            client.UpdateCell(1,2,"Name",SUBMITTED_CRITIQUES_KEY,worksheet_id)
            client.UpdateCell(1,3,"Stanford ID",SUBMITTED_CRITIQUES_KEY,worksheet_id)
            client.UpdateCell(1,4,"Reading",SUBMITTED_CRITIQUES_KEY,worksheet_id)
            client.UpdateCell(1,5,"Critique",SUBMITTED_CRITIQUES_KEY,worksheet_id)
            client.UpdateCell(1,6,"Grade",SUBMITTED_CRITIQUES_KEY,worksheet_id)
        
            full_papers.append((paper[0],worksheet_id,paper[1]))
            print "%d,%s,%s" % (paper[0],worksheet_id,paper[1])
            print "Adding sheet for paper %s" % paper[1]
    f=open(PAPERLIST,'w')
    pickle.dump(full_papers,f)
    f.close()




def populate_sheet(paper_index):
    '''Populates a specific paper index from the fresh critiques.'''
    f = open(PAPERLIST,'r')
    full_papers = pickle.load(f)
    f.close()
    paper = full_papers[paper_index-1]
    print "Populating critiques for paper : %s" % paper[2]

    query = gdata.spreadsheet.service.DocumentQuery()
    query['orderby'] = 'column:stanfordid_2'
    submissions = client.GetListFeed(FRESH_CRITIQUES_KEY,query=query)
    related_submissions = []
    for submission in submissions.entry:
        if(submission.custom['reading'].text == paper[2]):
            related_submissions.append(submission)

    for submission in related_submissions:
        # go and add this row to the other sheet...
        timestamp = submission.custom['timestamp'].text
        name = submission.custom['name'].text
        stanford_id = submission.custom['stanfordid_2'].text
        reading = submission.custom['reading'].text
        critique = submission.custom['critique'].text
        
        to_send = { "timestamp":timestamp, "name":name, "stanfordid":stanford_id,
                    "reading":reading,"critique":critique}
        client.InsertRow(to_send,SUBMITTED_CRITIQUES_KEY,paper[1])
        client.DeleteRow(submission)
        
    print "Moved %d related submissions!!" % len(related_submissions)

def list_papers():
    f = open(PAPERLIST,'r')
    full_papers = pickle.load(f)
    f.close()
    print "Index\tWorksheet_ID\tTitle\n"
    for paper in full_papers:
        print "%d\t%s\t\t%s" % (paper[0],paper[1],paper[2])

def list_submissions(paper_index):
    query = gdata.spreadsheet.service.DocumentQuery()
    query['orderby'] = 'column:stanfordid_2'

    if (paper_index != None):
        f = open(PAPERLIST,'r')
        full_papers = pickle.load(f)
        f.close()
        paper = full_papers[paper_index-1]
        print "Listing submissions for paper : %s\n" % paper[2]
#        sq_str = 'reading=%s' % paper[2]
#        print sq_str
#        query['sq'] = sq_str

    submissions = client.GetListFeed(FRESH_CRITIQUES_KEY, query=query)
    if paper_index == None:
        related_submissions = submissions.entry
    else:
        related_submissions = []
        for submission in submissions.entry:
            if (submission.custom['reading'].text == paper[2]):
                related_submissions.append(submission)

    i = 1
    for submission in related_submissions:
        print "%d:\t%s\t%s\t%s\t%s" % (i,submission.custom['timestamp'].text,
                                  submission.custom['stanfordid_2'].text,
                                  submission.custom['name'].text,
                                  submission.custom['reading'].text)
        i += 1

    last_suid = None
    i = 1
    for submission in related_submissions:
        if submission.custom['stanfordid_2'].text == last_suid:
            print "Detected duplicated entry for %d. %s(%s)" % (i,last_suid,submission.custom['name'].text)
        last_suid = submission.custom['stanfordid_2'].text
        i += 1


if __name__ =='__main__':
    usage = "usage: %prog [options] arg"
    description = "Simple Script to move sort CS244 critiques"
    parser = OptionParser(usage)
    parser.description = description
    parser.add_option("-l","--list",dest="list",
                      action="store_true",default=False,
                      help="List the papers to be critiqued")
    parser.add_option("-b","--list-submissions",dest="listsubs",
                      action="store_true",default=False,
                      help="List the fresh submissions")
    parser.add_option("-i","--index",dest="index",type="int",
                      default=None,
                      help="Populate the docs for paper with index i")
    parser.add_option("-s","--start",dest="start",action="store_true",
                      default=False,
                      help="Initialize the online spreadsheet, add worksheets.CAREFUL : This will change the doc!!!")
    parser.add_option("-n","--new-submissions",dest="new_sub",
                      action="store_true",default=False,
                      help="Add a new paper for submission")


    (options,args) = parser.parse_args()

    client = gdata.spreadsheet.service.SpreadsheetsService()
    client.ClientLogin(USERNAME, PASSWORD)
    
    if options.start == True:
        print "Initializing the online document!!"
        create_sheets()
        exit(0)

    if options.list == True:
        list_papers()
        exit(0)

    if options.listsubs == True:
        list_submissions(options.index)
        exit(0)

    if options.new_sub == True:
        add_new_paper(options.index)
        exit(0)

        
    if options.index != None:
        populate_sheet(options.index)
        exit(0)

