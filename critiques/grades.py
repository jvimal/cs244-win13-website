#!/usr/bin/env python
import gdata.spreadsheet.service
from string import find
from papers import papers
import string
import pickle
from optparse import OptionParser
import code

# Official Docs
STUDENTS_KEY=""

SUBMITTED_CRITIQUES_KEY="0AmavjfqTpYCadGdnSEtMRlhWQ2UxZkplcURFOGstZ1E"

USERNAME="cs244stanford"
PASSWORD="openflow"

PAPERLIST="paperlist.txt"

# Parse arguments
usage = "usage: %prog [options] arg"
description = "Simple Script to post grades for CS244"
parser = OptionParser(usage)
parser.description = description
parser.add_option("-l","--list",dest="list",
		  action="store_true",default=False,
		  help="List the papers to be critiqued")
parser.add_option("-i","--index",dest="index",type="int",
		  default=None, help="get grades for paper with index i")
parser.add_option("-a","--all",dest="all",action="store_true",default=False,
		  help="List grades for all students")
parser.add_option("-o","--outfile",dest="outfile",type="str",
		  default="grades.csv", help="output grades to csv file")
(options,args) = parser.parse_args()


def populate_sheet(paper_index):
    '''Populates a specific paper index from the fresh critiques.'''
    f = open(PAPERLIST,'r')
    full_papers = pickle.load(f)
    f.close()
    paper = full_papers[paper_index-1]
    print "Populating critiques for paper : %s" % paper[2]

    query = gdata.spreadsheet.service.DocumentQuery()
    query['orderby'] = 'column:stanfordid_2'
    submissions = client.GetListFeed(FRESH_CRITIQUES_KEY,query=query)
    related_submissions = []
    for submission in submissions.entry:
        if(submission.custom['reading'].text == paper[2]):
            related_submissions.append(submission)

    for submission in related_submissions:
        # go and add this row to the other sheet...
        print submission.custom.keys()
        timestamp = submission.custom['timestamp'].text
        name = submission.custom['name'].text
        stanford_id = submission.custom['stanfordid_2'].text
        reading = submission.custom['reading'].text
        critique = submission.custom['critique'].text
        
        to_send = { "timestamp":timestamp, "name":name, "stanfordid":stanford_id,
                    "reading":reading,"critique":critique}
        client.InsertRow(to_send,SUBMITTED_CRITIQUES_KEY,paper[1])
        client.DeleteRow(submission)
        
    print "Moved %d related submissions!!" % len(related_submissions)

def list_papers():
    f = open(PAPERLIST,'r')
    full_papers = pickle.load(f)
    f.close()
    print "Index\tWorksheet_ID\tTitle\n"
    for paper in full_papers:
        print "%d\t%s\t\t%s" % (paper[0],paper[1],paper[2])

def list_submissions(paper_index):
    query = gdata.spreadsheet.service.DocumentQuery()
    query['orderby'] = 'column:stanfordid_2'

    if (paper_index != None):
        f = open(PAPERLIST,'r')
        full_papers = pickle.load(f)
        f.close()
        paper = full_papers[paper_index-1]
        print "Listing submissions for paper : %s\n" % paper[2]
#        sq_str = 'reading=%s' % paper[2]
#        print sq_str
#        query['sq'] = sq_str

    submissions = client.GetListFeed(FRESH_CRITIQUES_KEY, query=query)
    if paper_index == None:
        related_submissions = submissions.entry
    else:
        related_submissions = []
        for submission in submissions.entry:
            if (submission.custom['reading'].text == paper[2]):
                related_submissions.append(submission)

    i = 1
    for submission in related_submissions:
        print "%d:\t%s\t%s\t%s\t%s" % (i,submission.custom['timestamp'].text,
                                  submission.custom['stanfordid_2'].text,
                                  submission.custom['name'].text,
                                  submission.custom['reading'].text)
        i += 1

    last_suid = None
    i = 1
    for submission in related_submissions:
        if submission.custom['stanfordid_2'].text == last_suid:
            print "Detected duplicated entry for %d. %s(%s)" % (i,last_suid,submission.custom['name'].text)
        last_suid = submission.custom['stanfordid_2'].text
        i += 1

def show_all_grades():
    students = {}
    f = open('enrolled_students.txt','r')
    for line in f:
        id = line.strip()
        students[id] = []
    f.close()

    f = open(PAPERLIST,'r')
    full_papers = pickle.load(f)
    f.close()

    for paper in full_papers:
        query = gdata.spreadsheet.service.DocumentQuery()
        query['orderby'] = 'column:stanfordid'
        submissions = client.GetListFeed(SUBMITTED_CRITIQUES_KEY,paper[1],query=query)
        for submission in submissions.entry:
            id = submission.custom['stanfordid'].text
            if students.has_key(id):
                students[id].append((paper[0],submission.custom['grade'].text))
            else:
                print "student not enrolled?? (%s,grade:%d)" % (id,int(submission.custom['grade'].text))
                                


    for id in students.keys():
        print "%s:%d critiques:%s" % (id,len(students[id]),students[id])
                                    
    for id in students.keys():
        if len(students[id]) == 0:
            print "student hasn't submitted anything - %s" % id
        

    


def show_paper_grades(paper_index):

    grades = {}
    names = {}
    f = open('enrolled_students.txt','r')
    for line in f:
        id = line.strip()
        grades[id] = 0
    f.close()

    f = open(PAPERLIST,'r')
    full_papers = pickle.load(f)
    f.close()
    paper = full_papers[paper_index-1]
    print "Listing Grades for paper : %s" % paper[2]

    out_f = open(options.outfile, 'w')
    print >>out_f, "%s" % paper[2]

    query = gdata.spreadsheet.service.DocumentQuery()
    query['orderby'] = 'column:stanfordid'
    submissions = client.GetListFeed(SUBMITTED_CRITIQUES_KEY,paper[1],query=query)
    print "Listing %d graded submissions\n" % len(submissions.entry)
    for submission in submissions.entry:
        id = submission.custom['stanfordid'].text
	name = submission.custom['name'].text
	names[id] = name
#        if submission.custom['grade'].text == None:
#            print "Warning : No grade posted for %s" % id
#        else:
        if grades.has_key(id):
            grades[id] = submission.custom['grade'].text
        else:
            print submission.custom['name'].text
            print submission.custom['stanfordid'].text
            print "Warning : Student %s (%s) not enrolled (grade : %s)...?" % (submission.custom['name'].text,
                                                                               submission.custom['stanfordid'].text, submission.custom['grade'].text)
    non_shows = 0
    non_grades = 0
#    print "Stanford IDs"
#    for id in grades.keys():
#        print id
#    print "Grades"
#    for id in grades.keys():
#        print grades[id]

    print "Names,Stanford IDs,Grades"
    for id in grades.keys():
	print >>out_f, "\"%s\",%s,%s" % (names.get(id, "NaN"), id, grades[id])
        print "\"%s\",%s,%s" % (names.get(id, "NaN"), id, grades[id])
        if grades[id] == 0:
            non_shows += 1
        if grades[id] == None:
            non_grades += 1
    out_f.close()

    print "%d students didn't submit a critique" % non_shows
    print "%d critiques not graded" % non_grades

if __name__ =='__main__':
    client = gdata.spreadsheet.service.SpreadsheetsService()
    client.ClientLogin(USERNAME, PASSWORD)
    
    if options.all == True:
        show_all_grades()
        exit(0)

    if options.list == True:
        list_papers()
        exit(0)

    if options.index != None:
        show_paper_grades(options.index)
        exit(0)

