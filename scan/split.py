#!/usr/bin/python

import os
import sys
import json
import string
from optparse import OptionParser

# Parse arguments
usage = "usage: %prog [options] arg"
description = "Simple Script to post grades for CS244"
parser = OptionParser(usage)
parser.description = description

# generic options
parser.add_option("-c","--conf",dest="conf",type="str",
		  default="answer.conf", help="config file")

# split by student
parser.add_option("-s","--student",dest="student",action="store_true",default=False,
		  help="Split by student")

# split by answer
parser.add_option("-a","--answer",dest="answer",action="store_true",default=False,
		  help="Split by answer")

# merge graded exams
parser.add_option("-m","--merge",dest="merge",action="store_true",default=False,
		  help="Merge graded exams")

(options,args) = parser.parse_args()

if __name__ == '__main__':

    c = open(options.conf, "r")
    conf = json.load(c)

    infile = conf["all"]["infile"]
    mergedir = conf["all"]["mergedir"]
    num_pages = conf["all"]["num_pages"]
    ans_len = conf["all"]["ans_len"]
    numfiles = num_pages/ans_len
    questions = conf["questions"]

    if options.student:
	outdir = conf["all"]["outdir"]
	if not os.path.isdir(outdir):
	    os.makedirs(outdir)
	print 'infile: %s' % infile
	print 'num_pages: %d' % num_pages
	print 'outdir: %s' % outdir
	for i in range(numfiles):
	    minp = 1 + i*20
	    maxp = 20*(i+1)
	    print 'File no. %d' % (i+1)
	    os.system('pdftk %s cat %d-%d output %s/%d.pdf' % (infile, minp,
		maxp, outdir, i+1))
    elif options.answer:	    
	indir = conf["all"]["outdir"]
	for q in questions:
	    outdir = q["outdir"]
	    if not os.path.isdir(outdir):
		os.makedirs(outdir)
	    print "*** Output dir: %s ***" % outdir
	    for i in range(numfiles):
		infile = "%s/%d.pdf" % (indir, i+1)
		outfile = "%s/%d.pdf" % (outdir, i+1)
		print 'File no. %d: %s -> %s' % (i+1, infile, outfile)
		os.system('pdftk %s cat %s output %s' % (infile,
		    q["page_range"], outfile))
	    print "-------------------------------------------------"
    elif options.merge:
	outdir = mergedir
	if not os.path.isdir(outdir):
	    os.makedirs(outdir)
	for i in range(numfiles):
	    allchars = string.ascii_uppercase
	    infiles = "A=%s/%d.pdf" % (conf["all"]["outdir"], i+1)
	    merge_pages = "A%s" % conf["all"]["cover_pages"]
	    char_index = 1
	    for q in questions:
		infiles += " %s=%s/%d.pdf" % (allchars[char_index], q["outdir"], i+1)
		merge_pages += " %s" % allchars[char_index]
		char_index += 1
	    outfile = "%s/%d.pdf" % (outdir, i+1)
	    print 'File no. %d: %s' % (i+1, outfile)
	    os.system("pdftk %s cat %s output %s" % (infiles, merge_pages, outfile))
